package com.example.p3c2_java;

import android.app.Activity;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.core.view.MenuItemCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Modelo.AlumnosDb;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fbtnAgregar, fbtnCerrar;
    private Aplicacion app;
    private Alumno alumno;
    private int posicion;
    private String currentSearchQuery = "";
    private AlumnosDb alumnosDb;
    private MenuItem searchMenuItem ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.inicializarComponentes();
        this.fbtnAgregar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) { fbtnAgregar(); }
        });
        this.fbtnCerrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) { fbtnCerrar(); }
        });

        app.getAdaptador().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { recyclerView(view); }
        });
    }

    private void inicializarComponentes(){
        this.alumno = new Alumno();
        this.posicion = -1;
        this.app = (Aplicacion) getApplication();
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView = findViewById(R.id.recId);
        this.recyclerView.setAdapter(app.getAdaptador());
        this.recyclerView.setLayoutManager(this.layoutManager);

        this.fbtnAgregar = findViewById(R.id.agregarAlumno);
        this.fbtnCerrar = findViewById(R.id.cerrar);
        this.alumnosDb = new AlumnosDb(getApplicationContext());

    }

    private void fbtnAgregar(){
        this.alumno = null;
        Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("alumno", this.alumno);
        bundle.putInt("posicion", this.posicion);
        intent.putExtras(bundle);
        startActivityForResult(intent, 0);
    }

    private void fbtnCerrar(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Estás seguro de cerrar la aplicación?")
                .setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }

    private void recyclerView(View view){
        int posicion = recyclerView.getChildAdapterPosition(view);
        alumno = app.getAlumnos().get(posicion);

        Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("alumno", alumno);
        bundle.putInt("posicion", posicion);
        intent.putExtras(bundle);

        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == Activity.RESULT_OK) {
            app.getAlumnos().clear();
            app.getAlumnos().addAll(alumnosDb.allAlumnos());
            app.getAdaptador().setAlumnoList(app.getAlumnos());
            recreate();
            // app.getAdaptador().setAlumnoList(app.getAlumnos());
        }

        this.posicion = -1;
        this.alumno = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.searchview, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                currentSearchQuery = newText;
                app.getAdaptador().getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

