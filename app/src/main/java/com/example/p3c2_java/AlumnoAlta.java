package com.example.p3c2_java;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.io.IOException;

import Modelo.AlumnoDbHelper;
import Modelo.AlumnosDb;


public class AlumnoAlta extends AppCompatActivity {

    private Button btnGuardar, btnRegresar, btnEliminar;
    private Alumno alumno;
    private EditText txtNombre, txtMatricula, txtCarrera, txtImagenUrl;
    private ImageView imgAlumno;
    private int posicion;
    private TextView lblImagen;
    private AlumnosDb alumnosDb;

    private static final int PICK_IMAGE_REQUEST = 1;
    String path = "https://images.pexels.com/photos/2023384/pexels-photo-2023384.jpeg?auto=compress&cs=tinysrgb&w=1600";
    File file = new File(Environment.getExternalStorageDirectory(), "siiaa.png");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno_alta);

        alumnosDb = new AlumnosDb(getApplicationContext());

        this.inicializarComponentes();

        Bundle bundle = getIntent().getExtras();
        this.alumno = (Alumno) bundle.getSerializable("alumno");
        this.posicion = bundle.getInt("posicion", posicion);

        btnEliminar.setVisibility(View.GONE);

        if (posicion >= 0 && alumno != null) {
            txtMatricula.setText(alumno.getMatricula());
            txtNombre.setText(alumno.getNombre());
            txtCarrera.setText(alumno.getCarrera());
            imgAlumno.setImageResource(alumno.getImg());
            txtImagenUrl.setText(alumno.getImagenUrl());
            if (!alumno.getImagenUrl().isEmpty()) {
                loadAlumnoImage(alumno.getImagenUrl());
            }
            btnEliminar.setVisibility(View.VISIBLE);
        }

        this.btnGuardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                btnGuardar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                btnRegresar();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() { public void onClick(View view) { btnEliminar(); }
        });

        imgAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_IMAGE_REQUEST);
            }
        });

    }

    public void inicializarComponentes() {
        btnGuardar = findViewById(R.id.btnSalir);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtMatricula = findViewById(R.id.txtMatricula);
        txtNombre = findViewById(R.id.txtNombre);
        txtCarrera = findViewById(R.id.txtCarrera);
        imgAlumno = findViewById(R.id.imgAlumno);
        btnEliminar = findViewById(R.id.btnEliminar);
        txtImagenUrl = findViewById(R.id.txtImagenUrl);
    }

    private void btnGuardar() {
        Aplicacion app = (Aplicacion) getApplication();
        if (validar()) {
            if (alumno == null) {
                // Código para guardar un nuevo alumno
                alumno = new Alumno();
                alumno.setCarrera(txtCarrera.getText().toString());
                alumno.setMatricula(txtMatricula.getText().toString());
                alumno.setNombre(txtNombre.getText().toString());
                alumno.setImagenUrl(txtImagenUrl.getText().toString());

                String imageUrl = txtImagenUrl.getText().toString();
                loadAlumnoImage(imageUrl);

                alumno.setImagenUrl(imageUrl);
                long resultado = alumnosDb.insertAlumno(alumno);
                if (resultado != -1) {
                    alumno.setId((int) resultado);
                    Toast.makeText(getApplicationContext(), "El alumno se agregó correctamente", Toast.LENGTH_SHORT).show();
                }

                app.getAlumnos().add(alumno);
                setResult(Activity.RESULT_OK);
                Toast.makeText(getApplicationContext(), "El alumno se agregó correctamente", Toast.LENGTH_SHORT).show();
            } else {
                // Código para actualizar un alumno existente
                alumno.setMatricula(txtMatricula.getText().toString());
                alumno.setNombre(txtNombre.getText().toString());
                alumno.setCarrera(txtCarrera.getText().toString());
                alumno.setImagenUrl(txtImagenUrl.getText().toString());

                Alumno alumnoActualizado = app.getAlumnos().get(posicion);
                alumnoActualizado.setMatricula(alumno.getMatricula());
                alumnoActualizado.setNombre(alumno.getNombre());
                alumnoActualizado.setCarrera(alumno.getCarrera());
                alumnoActualizado.setImagenUrl(alumno.getImagenUrl());

                alumnosDb.updateAlumno(alumno);
                setResult(Activity.RESULT_OK);
                Toast.makeText(getApplicationContext(), "El alumno se actualizó correctamente", Toast.LENGTH_SHORT).show();
            }

            finish();
            app.getAdaptador().notifyItemChanged(posicion);
        } else {
            Toast.makeText(getApplicationContext(), "Faltó capturar datos", Toast.LENGTH_SHORT).show();
            txtMatricula.requestFocus();
        }
    }


    private void btnRegresar() {
        finish();
    }
    private void btnEliminar() {
        Aplicacion app = (Aplicacion) getApplication();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Estás seguro de eliminar a este alumno?")
                .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Alumno alumnoAEliminar = app.getAlumnos().get(posicion);
                        alumno.setMatricula(txtMatricula.getText().toString());
                        alumno.setNombre(txtNombre.getText().toString());
                        alumno.setCarrera(txtCarrera.getText().toString());

                        Aplicacion app = (Aplicacion) getApplication(); // Eliminar el alumno de la lista
                        AlumnosDb alumnosDb = new AlumnosDb(getApplicationContext());
                        alumnosDb.deleteAlumno(alumnoAEliminar.getId());
                        app.getAlumnos().remove(posicion);
                        Toast.makeText(getApplicationContext(), "El alumno se eliminó correctamente", Toast.LENGTH_SHORT).show();
                        setResult(Activity.RESULT_OK);
                        finish();
                        app.getAdaptador().notifyDataSetChanged(); // Notificar al adaptador del cambio en la lista
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }
    private boolean validar() {
        boolean exito = true;
        Log.d("nombre", "validar: " + txtNombre.getText());
        if (txtNombre.getText().toString().isEmpty()) exito = false;
        if (txtMatricula.getText().toString().isEmpty()) exito = false;
        if (txtCarrera.getText().toString().isEmpty()) exito = false;
        if (txtImagenUrl.getText().toString().isEmpty()) exito = false;

        return exito;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getData() != null) {
                Uri imageUri = data.getData();
                Glide.with(this)
                        .load(imageUri)
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .placeholder(R.drawable.agus)
                        .error(R.drawable.close)
                        .into(imgAlumno);
                txtImagenUrl.setText(imageUri.toString()); // Actualizar la URL en el campo de texto
            }
        }
    }


    private void loadAlumnoImage(String imageUrl) {
        Glide.with(this)
                .load(imageUrl)
                // .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)) // Opcional: Comenta esta línea si quieres usar la configuración predeterminada de Glide para la caché
                .placeholder(R.drawable.usuario)
                .error(R.drawable.close)
                .into(imgAlumno);
    }
}