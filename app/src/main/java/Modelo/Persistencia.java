package Modelo;

import com.example.p3c2_java.Alumno;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertAlumno (Alumno alumno);
    public long updateAlumno (Alumno alumno);
    public void deleteAlumno(int id);
}
